export class RadioService {
    items:Array<any>;

    constructor() {
        this.items = [
            { id: 1, name: 'Rihanna' },
            { id: 2, name: 'beyonce' },
            { id: 3, name: 'shakira' },
            { id: 4, name: 'Miley Cyrus' },
            { id: 5, name: 'selena gomez' }
        ];
    }

    getItems() {
        return this.items;
    }
}