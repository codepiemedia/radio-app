import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';

import { RadioService } from '../services/radio.service';
import { RadioDetailPage } from './radio-detail.ctrl';


@Component({
	templateUrl: '../views/radio-list.html',
	providers: [RadioService]
})
export class RadioListPage {
	radioPlayList: any[];

	constructor(
		public radioService: RadioService, 
		public navCtrl: NavController
	) {
		this.radioPlayList = [];
		this.radioPlayList = radioService.getItems();
	}

	radioSelected () {
		this.navCtrl.push(RadioDetailPage);
	}
}
