import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, NavController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';


import { DashboardPage } from './modules/dashboard/dashboard';
import { RadioListPage } from './modules/radio/controllers/radio-list.ctrl';

@Component({
    templateUrl: 'app.html'
})


export class MyApp {
    @ViewChild('myNav') nav: NavController;
    rootPage: any = DashboardPage;

    constructor(
        public platform: Platform,
        public menu: MenuController
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            StatusBar.styleDefault();
            Splashscreen.hide();
        });
    }

    goToPage(from: String) {
        if (from === 'Dashboard') {
            this.nav.setRoot(DashboardPage);
        }
        if (from === 'Radio') {
            this.nav.setRoot(RadioListPage);
        }
        this.menu.close();
    }
}