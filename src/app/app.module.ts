import { NgModule } from '@angular/core';
import { IonicApp, DeepLinkConfig, IonicModule } from 'ionic-angular';
import { APP_BASE_HREF } from '@angular/common';


import { MyApp } from './app.component';
import { DashboardPage } from './modules/dashboard/dashboard';
import { RadioListPage } from './modules/radio/controllers/radio-list.ctrl';
import { RadioDetailPage } from './modules/radio/controllers/radio-detail.ctrl';


export const deepLinkConfig: DeepLinkConfig = {
    links: [
        { name: 'dashboard', segment: '', component: DashboardPage },
        { name: 'Radio-list', segment: 'radio-list', component: RadioListPage },
        { 
            name: 'Radio-detail', 
            segment: 'radio-detail', 
            component: RadioDetailPage
        }
    ]
};


@NgModule({
    declarations: [
        MyApp,
        DashboardPage,
        RadioListPage,
        RadioDetailPage
    ],
    imports: [
        IonicModule.forRoot(MyApp, { locationStrategy: 'path' }, deepLinkConfig)
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        DashboardPage,
        RadioListPage,
        RadioDetailPage
    ],
    providers: [
        {provide: APP_BASE_HREF, useValue: '/'}
    ]
})
export class AppModule {}
